// importar o módulo do framework express
var express = require('express');

// importar o modulo do framework consign 
var consign = require('consign');

// importar o modulo do framework body-parser
var body_parser = require('body-parser');

// importar o modulo do express-validator
var express_validator = require('express-validator');

/* importar o módulo do express-session */
var expressSession = require('express-session');

// Inicializar o objeto do express
var app = express();

// configurar as variaveis 'view engine' e 'views' do express
app.set('view engine', 'ejs');
app.set('views', './app/views');

// configurar os middlewares express.static
app.use(express.static('./app/public'));

// configurar o middleware Body-Parser
app.use(body_parser.urlencoded({extended : true}));

// configurar o middleware Express-Validator
app.use(express_validator());

/* configurar o middleware express-session */
app.use(expressSession({
	secret: 'csNJS2016br777',
	resave: false,
	saveUnitialized: false
}));

// Efetua o autoload das rotas, dos models e dos controllers para o objeto [app]
consign()
    .include('app/routes')
    .then('app/models')
    .then('app/controllers')
    .then('config/dbConnection.js')
    .into(app); // <-- Objeto [app]


// exportar o objeto app
module.exports = app;