var mongo = require('mongodb');

var connMongoDb = function(){
    console.log('entrou db');
        /**
     *  a nova instancia do servidor de bd 
     *  recebe 3 parametros
     *  1 - Nome do banco de dados
     *  2 - Um objeto mongo.Server
     *  3 - Um jason com configurações adicionais 
     */
    var db = new mongo.Db(
        'mly_room_chat',
        /**
         * A nova instancia do servidor do mongo
         * também requer 3 parametros
         * 1 - o nome do local do servidor
         * 2 - porta de conexão do server
         * 3 - um json de configurações adicionais
         */
        new mongo.Server(
            'localhost',
            27017,
            {}
        ),
        {}
    );

    return db;
}

module.exports = function(){
    return connMongoDb;
}// fim func