module.exports = function(application){
	application.post('/chat', function(req, res){
		application.app.controllers.chat_ctrl.iniciaChat(application, req, res);
	});

	application.get('/chat', function(req, res){
		application.app.controllers.chat_ctrl.iniciaChat(application, req, res);
	});

	application.get('/sair', function(req, res){
		application.app.controllers.chat_ctrl.sair(application, req, res);
	});
}// fim da func 