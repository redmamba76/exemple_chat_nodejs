module.exports.cadastro = function(application, req, res){
    res.render('cadastro', { validacao: {}, dadosForm: {} } );
}// fim cadastro

module.exports.cadastrar = function(application, req, res){
    
    var dadosFormS = req.body;

    console.log("deu certo! cadastrar");

    req.assert('nome','Nomé é obrigatório').notEmpty();
    req.assert('sobrenome','Sobrenome é obrigatório').notEmpty();
    req.assert('email','Email é obrigatório').notEmpty();
    req.assert('email','Email não válido').isEmail();
    req.assert('senha','Senha é obrigatória').notEmpty();
    req.assert('confirmar_senha','Confirmar a senha é necessário').notEmpty();

    var errors = req.validationErrors();

    if(dadosFormS.senha != dadosFormS.confirmar_senha){

        res.render('cadastro', {validacao: {msg: 'As senhas tem que ser iguais para confirmar cadastro'}} );
    }

    if(errors){
        res.render('cadastro', {validacao: errors, dadosForm : dadosFormS, msg: 'Cadastro não efetuado' } );
        return;
    }// fim if

    

    var connection = application.config.dbConnection;
    var UsuarioDAO = new application.app.models.UsuarioDAO( connection );
    console.log('Cadastro efetuado com sucesso' + dadosFormS.usuario);
    UsuarioDAO.inserirUsuario(dadosFormS);
    res.render("index", {validacao : {} , msg: 'S'} );
}// fim cadastrar