# Multiroom Chat - Socket.io NodeJS and Bootstrap

## Installing the project

Clone the project on your machine open a terminal and run `npm i` to install all dependencies.

After installing all dependencies open terminal and run `node app.js` this command will start node server and enable [localhost:8080](http://localhost:8080) to enter the application.

## Technologies

For this example project I am using NodeJS, Express, Socket.io and Bootstrap as styles.

## About this project

This was part of a trainning course focused on NodeJS and MongoDB.
