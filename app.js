// importar as configurações do servidor
var app = require('./config/server');

// parametrizar a porta de escuta
var server = app.listen(8080, function(){
    console.log('Server ON');
});

var io = require('socket.io').listen(server);

/**
 *  Adiciona variavel [ io ] no escopo global
 * @param io variavel com as configurações do
 * server com o socket.io
 */
app.set('io', io);

// Criar conexão por websocket
io.on('connection', function(socket){
    
    console.log('Usuário conectou');

    socket.on('disconnect', function(){

        console.log('Usuário desconectou');

    });// fim do on 

    socket.on('msgParaServidor', function ( data ) {

        // ###### DIALOGO ######
        socket.emit(
            'msgParaCliente', 
            {apelido : data.apelido, mensagem : data.mensagem}
        );// fim do emit

        // Função broadcast envia dados para todos
        // menos para o emissor
        socket.broadcast.emit(
            'msgParaCliente', 
            {apelido : data.apelido, mensagem : data.mensagem}
        );// fim do emiT
        

        // ###### PARTICIPANTES ######
        // lista de pessoas online 
        

        // Função emit envia dados para 
        // o emissor, mas não outros clientes
        socket.emit(
            'participantesParaCliente', 
            {apelido : data.apelido}
        );// fim do emit

        // Função broadcast envia dados para todos
        // menos para o emissor
        socket.broadcast.emit(
            'participantesParaCliente', 
            {apelido : data.apelido}
        );// fim do emit
            

    });// fim da func 
});// fim de tudo